<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Historial extends Model {

	protected $table= "historials";

	public function getUser()
    {
        return $this->hasOne('App\User', 'id', 'id_user');
    }
    public function getLead()
    {
        return $this->hasOne('App\Lead', 'id', 'id_lead');
    }

}
