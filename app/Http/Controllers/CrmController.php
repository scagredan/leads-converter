<?php namespace App\Http\Controllers;

use App\Http\Controllers\Controller;

use Request;
use App\Lead;
use App\Historial;
use Auth;

class CrmController extends Controller {


	public function __construct()
	{
		$this->middleware('auth');
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{

		switch (Auth::user()->roll) {
			case 1:
			if (Request::input('fecha')) {

				// dd(Request::input('fecha'));
				$fecha1 = \DateTime::createFromFormat('d/m/Y H:i',Request::input('fecha').' 00:00');
				$fecha2 = \DateTime::createFromFormat('d/m/Y H:i',Request::input('fecha').' 23:59');
				$nuevos = Lead::where('estado','=',1)->whereBetween('created_at', [$fecha1, $fecha2])->get();
				$cnuevos = Lead::where('estado','=',1)->whereBetween('created_at', [$fecha1, $fecha2])->count();
				$duplicados = Lead::where('estado','=',2)->whereBetween('created_at', [$fecha1, $fecha2])->get();
				$cduplicados = Lead::where('estado','=',2)->whereBetween('created_at', [$fecha1, $fecha2])->count();
				$errados = Lead::where('estado','=',3)->whereBetween('created_at', [$fecha1, $fecha2])->get();
				$cerrados = Lead::where('estado','=',3)->whereBetween('created_at', [$fecha1, $fecha2])->count();
				$agendados = Lead::where('estado','=',4)->whereBetween('created_at', [$fecha1, $fecha2])->get();
				$cagendados = Lead::where('estado','=',4)->whereBetween('created_at', [$fecha1, $fecha2])->count();
				$finalizados = Lead::where('estado','=',5)->whereBetween('updated_at', [$fecha1, $fecha2])->get();
				$cfinalizados = Lead::where('estado','=',5)->whereBetween('updated_at', [$fecha1, $fecha2])->count();
				$nfinalizados = Lead::where('estado','=',6)->whereBetween('created_at', [$fecha1, $fecha2])->get();
				$cnfinalizados = Lead::where('estado','=',6)->whereBetween('created_at', [$fecha1, $fecha2])->count();
				$ninteresados = Lead::where('estado','=',7)->whereBetween('created_at', [$fecha1, $fecha2])->get();
				$cninteresados = Lead::where('estado','=',7)->whereBetween('created_at', [$fecha1, $fecha2])->count();
				$pendiente = Lead::where('estado','=',8)->whereBetween('created_at', [$fecha1, $fecha2])->get();
				$cpendiente = Lead::where('estado','=',8)->whereBetween('created_at', [$fecha1, $fecha2])->count();
				$pendientec = Lead::where('estado','=',9)->whereBetween('created_at', [$fecha1, $fecha2])->get();
				$cpendientec = Lead::where('estado','=',9)->whereBetween('created_at', [$fecha1, $fecha2])->count();
			}else 
			if (Request::input('fecha1') && Request::input('fecha2')) {
				$fecha1 = \DateTime::createFromFormat('d/m/Y H:i',Request::input('fecha1').' 00:00');
				$fecha2 = \DateTime::createFromFormat('d/m/Y H:i',Request::input('fecha2').' 23:59');
				$nuevos = Lead::where('estado','=',1)->whereBetween('created_at', [$fecha1, $fecha2])->get();
				$cnuevos = Lead::where('estado','=',1)->whereBetween('created_at', [$fecha1, $fecha2])->count();
				$duplicados = Lead::where('estado','=',2)->whereBetween('created_at', [$fecha1, $fecha2])->get();
				$cduplicados = Lead::where('estado','=',2)->whereBetween('created_at', [$fecha1, $fecha2])->count();
				$errados = Lead::where('estado','=',3)->whereBetween('created_at', [$fecha1, $fecha2])->get();
				$cerrados = Lead::where('estado','=',3)->whereBetween('created_at', [$fecha1, $fecha2])->count();
				$agendados = Lead::where('estado','=',4)->whereBetween('created_at', [$fecha1, $fecha2])->get();
				$cagendados = Lead::where('estado','=',4)->whereBetween('created_at', [$fecha1, $fecha2])->count();
				$finalizados = Lead::where('estado','=',5)->whereBetween('updated_at', [$fecha1, $fecha2])->get();
				$cfinalizados = Lead::where('estado','=',5)->whereBetween('updated_at', [$fecha1, $fecha2])->count();
				$nfinalizados = Lead::where('estado','=',6)->whereBetween('created_at', [$fecha1, $fecha2])->get();
				$cnfinalizados = Lead::where('estado','=',6)->whereBetween('created_at', [$fecha1, $fecha2])->count();
				$ninteresados = Lead::where('estado','=',7)->whereBetween('created_at', [$fecha1, $fecha2])->get();
				$cninteresados = Lead::where('estado','=',7)->whereBetween('created_at', [$fecha1, $fecha2])->count();
				$pendiente = Lead::where('estado','=',8)->whereBetween('created_at', [$fecha1, $fecha2])->get();
				$cpendiente = Lead::where('estado','=',8)->whereBetween('created_at', [$fecha1, $fecha2])->count();
				$pendientec = Lead::where('estado','=',9)->whereBetween('created_at', [$fecha1, $fecha2])->get();
				$cpendientec = Lead::where('estado','=',9)->whereBetween('created_at', [$fecha1, $fecha2])->count();
			}else{
				$nuevos = Lead::where('estado','=',1)->get();
				$cnuevos = Lead::where('estado','=',1)->count();
				$duplicados = Lead::where('estado','=',2)->get();
				$cduplicados = Lead::where('estado','=',2)->count();
				$errados = Lead::where('estado','=',3)->get();
				$cerrados = Lead::where('estado','=',3)->count();
				$agendados = Lead::where('estado','=',4)->get();
				$cagendados = Lead::where('estado','=',4)->count();
				$finalizados = Lead::where('estado','=',5)->get();
				$cfinalizados = Lead::where('estado','=',5)->count();
				$nfinalizados = Lead::where('estado','=',6)->get();
				$cnfinalizados = Lead::where('estado','=',6)->count();
				$ninteresados = Lead::where('estado','=',7)->get();
				$cninteresados = Lead::where('estado','=',7)->count();
				$pendiente = Lead::where('estado','=',8)->get();
				$cpendiente = Lead::where('estado','=',8)->count();
				$pendientec = Lead::where('estado','=',9)->get();
				$cpendientec = Lead::where('estado','=',9)->count();	
			}
			
			
			return view('crm')->with(['nuevos'=> $nuevos, 'cnuevos'=>$cnuevos,'duplicados'=> $duplicados, 'cduplicados'=>$cduplicados,'errados'=> $errados, 'cerrados'=>$cerrados,'agendados'=> $agendados, 'cagendados'=>$cagendados,'finalizados'=> $finalizados, 'cfinalizados'=>$cfinalizados, 'nfinalizados'=> $nfinalizados, 'cnfinalizados'=>$cnfinalizados, 'ninteresados'=> $ninteresados , 'cninteresados'=> $cninteresados,'pendientes'=> $pendiente,'cpendientes'=>$cpendiente,'pendientesc'=> $pendientec,'cpendientesc'=>$cpendientec]);
			break;
			case 2:
			if (Request::input('fecha')) {

				$fecha1 = \DateTime::createFromFormat('d/m/Y H:i',Request::input('fecha').' 00:00');
				$fecha2 = \DateTime::createFromFormat('d/m/Y H:i',Request::input('fecha').' 23:59');
				$agendados = Lead::where('estado','=',4)->where('id_user', '=', Auth::user()->id)->whereBetween('created_at', [$fecha1, $fecha2])->get();
				$cagendados = Lead::where('estado','=',4)->where('id_user', '=', Auth::user()->id)->whereBetween('created_at', [$fecha1, $fecha2])->count();
				$finalizados = Lead::where('estado','=',5)->where('id_user', '=', Auth::user()->id)->whereBetween('updated_at', [$fecha1, $fecha2])->get();
				$cfinalizados = Lead::where('estado','=',5)->where('id_user', '=', Auth::user()->id)->whereBetween('updated_at', [$fecha1, $fecha2])->count();
				$nfinalizados = Lead::where('estado','=',6)->where('id_user', '=', Auth::user()->id)->whereBetween('created_at', [$fecha1, $fecha2])->get();
				$cnfinalizados = Lead::where('estado','=',6)->where('id_user', '=', Auth::user()->id)->whereBetween('created_at', [$fecha1, $fecha2])->count();
				$pendientec = Lead::where('estado','=',9)->where('id_user', '=', Auth::user()->id)->whereBetween('created_at', [$fecha1, $fecha2])->get();
				$cpendientec = Lead::where('estado','=',9)->where('id_user', '=', Auth::user()->id)->whereBetween('created_at', [$fecha1, $fecha2])->count();

			}else
			if (Request::input('fecha1') && Request::input('fecha2')) {
				$fecha1 = \DateTime::createFromFormat('d/m/Y H:i',Request::input('fecha1').' 00:00');
				$fecha2 = \DateTime::createFromFormat('d/m/Y H:i',Request::input('fecha2').' 23:59');
				$agendados = Lead::where('estado','=',4)->where('id_user', '=', Auth::user()->id)->whereBetween('created_at', [$fecha1, $fecha2])->get();
				$cagendados = Lead::where('estado','=',4)->where('id_user', '=', Auth::user()->id)->whereBetween('created_at', [$fecha1, $fecha2])->count();
				$finalizados = Lead::where('estado','=',5)->where('id_user', '=', Auth::user()->id)->whereBetween('updated_at', [$fecha1, $fecha2])->get();
				$cfinalizados = Lead::where('estado','=',5)->where('id_user', '=', Auth::user()->id)->whereBetween('updated_at', [$fecha1, $fecha2])->count();
				$nfinalizados = Lead::where('estado','=',6)->where('id_user', '=', Auth::user()->id)->whereBetween('created_at', [$fecha1, $fecha2])->get();
				$cnfinalizados = Lead::where('estado','=',6)->where('id_user', '=', Auth::user()->id)->whereBetween('created_at', [$fecha1, $fecha2])->count();
				$pendientec = Lead::where('estado','=',9)->where('id_user', '=', Auth::user()->id)->whereBetween('created_at', [$fecha1, $fecha2])->get();
				$cpendientec = Lead::where('estado','=',9)->where('id_user', '=', Auth::user()->id)->whereBetween('created_at', [$fecha1, $fecha2])->count();
			}else{

				$agendados = Lead::where('estado','=',4)->where('id_user', '=', Auth::user()->id)->get();
				$cagendados = Lead::where('estado','=',4)->where('id_user', '=', Auth::user()->id)->count();
				$finalizados = Lead::where('estado','=',5)->where('id_user', '=', Auth::user()->id)->get();
				$cfinalizados = Lead::where('estado','=',5)->where('id_user', '=', Auth::user()->id)->count();
				$nfinalizados = Lead::where('estado','=',6)->where('id_user', '=', Auth::user()->id)->get();
				$cnfinalizados = Lead::where('estado','=',6)->where('id_user', '=', Auth::user()->id)->count();
				$pendientec = Lead::where('estado','=',9)->where('id_user', '=', Auth::user()->id)->get();
				$cpendientec = Lead::where('estado','=',9)->where('id_user', '=', Auth::user()->id)->count();
			}
			return view('crmcoordinador')->with(['agendados'=> $agendados, 'cagendados'=>$cagendados,'finalizados'=> $finalizados, 'cfinalizados'=>$cfinalizados, 'nfinalizados'=> $nfinalizados, 'cnfinalizados'=>$cnfinalizados,'pendientesc'=> $pendientec,'cpendientesc'=>$cpendientec]);
			break;

			case 3:
			if (Request::input('fecha')) {

				// dd(Request::input('fecha'));
				$fecha1 = \DateTime::createFromFormat('d/m/Y H:i',Request::input('fecha').' 00:00');
				$fecha2 = \DateTime::createFromFormat('d/m/Y H:i',Request::input('fecha').' 23:59');
				$nuevos = Lead::where('estado','=',1)->whereBetween('created_at', [$fecha1, $fecha2])->get();
				$cnuevos = Lead::where('estado','=',1)->whereBetween('created_at', [$fecha1, $fecha2])->count();
				$duplicados = Lead::where('estado','=',2)->whereBetween('created_at', [$fecha1, $fecha2])->get();
				$cduplicados = Lead::where('estado','=',2)->whereBetween('created_at', [$fecha1, $fecha2])->count();
				$errados = Lead::where('estado','=',3)->whereBetween('created_at', [$fecha1, $fecha2])->get();
				$cerrados = Lead::where('estado','=',3)->whereBetween('created_at', [$fecha1, $fecha2])->count();
				$agendados = Lead::where('estado','=',4)->whereBetween('created_at', [$fecha1, $fecha2])->where('callcenter','=', Auth::user()->id)->get();
				$cagendados = Lead::where('estado','=',4)->whereBetween('created_at', [$fecha1, $fecha2])->where('callcenter','=', Auth::user()->id)->count();
				$ninteresados = Lead::where('estado','=',7)->whereBetween('created_at', [$fecha1, $fecha2])->get();
				$cninteresados = Lead::where('estado','=',7)->whereBetween('created_at', [$fecha1, $fecha2])->count();
				$pendiente = Lead::where('estado','=',8)->whereBetween('created_at', [$fecha1, $fecha2])->where('callcenter','=', Auth::user()->id)->get();
				$cpendiente = Lead::where('estado','=',8)->whereBetween('created_at', [$fecha1, $fecha2])->where('callcenter', '=', Auth::user()->id)->count();
			}else
			if (Request::input('fecha1') && Request::input('fecha2')) {
				$fecha1 = \DateTime::createFromFormat('d/m/Y H:i',Request::input('fecha1').' 00:00');
				$fecha2 = \DateTime::createFromFormat('d/m/Y H:i',Request::input('fecha2').' 23:59');
				$nuevos = Lead::where('estado','=',1)->whereBetween('created_at', [$fecha1, $fecha2])->get();
				$cnuevos = Lead::where('estado','=',1)->whereBetween('created_at', [$fecha1, $fecha2])->count();
				$duplicados = Lead::where('estado','=',2)->whereBetween('created_at', [$fecha1, $fecha2])->get();
				$cduplicados = Lead::where('estado','=',2)->whereBetween('created_at', [$fecha1, $fecha2])->count();
				$errados = Lead::where('estado','=',3)->whereBetween('created_at', [$fecha1, $fecha2])->get();
				$cerrados = Lead::where('estado','=',3)->whereBetween('created_at', [$fecha1, $fecha2])->count();
				$agendados = Lead::where('estado','=',4)->whereBetween('created_at', [$fecha1, $fecha2])->where('callcenter','=', Auth::user()->id)->get();
				$cagendados = Lead::where('estado','=',4)->whereBetween('created_at', [$fecha1, $fecha2])->where('callcenter','=', Auth::user()->id)->count();
				$ninteresados = Lead::where('estado','=',7)->whereBetween('created_at', [$fecha1, $fecha2])->get();
				$cninteresados = Lead::where('estado','=',7)->whereBetween('created_at', [$fecha1, $fecha2])->count();
				$pendiente = Lead::where('estado','=',8)->whereBetween('created_at', [$fecha1, $fecha2])->where('callcenter','=', Auth::user()->id)->get();
				$cpendiente = Lead::where('estado','=',8)->whereBetween('created_at', [$fecha1, $fecha2])->where('callcenter', '=', Auth::user()->id)->count();
			}else{
				$nuevos = Lead::where('estado','=',1)->get();
				$cnuevos = Lead::where('estado','=',1)->count();
				$duplicados = Lead::where('estado','=',2)->get();
				$cduplicados = Lead::where('estado','=',2)->count();
				$errados = Lead::where('estado','=',3)->get();
				$cerrados = Lead::where('estado','=',3)->count();
				$agendados = Lead::where('estado','=',4)->where('callcenter','=', Auth::user()->id)->get();
				$cagendados = Lead::where('estado','=',4)->where('callcenter','=', Auth::user()->id)->count();
				$ninteresados = Lead::where('estado','=',7)->get();
				$cninteresados = Lead::where('estado','=',7)->count();
				$pendiente = Lead::where('estado','=',8)->where('callcenter','=', Auth::user()->id)->get();
				$cpendiente = Lead::where('estado','=',8)->where('callcenter', '=', Auth::user()->id)->count();
			}

			
			return view('crmcallcenter')->with(['nuevos'=> $nuevos, 'cnuevos'=>$cnuevos,'duplicados'=> $duplicados, 'cduplicados'=>$cduplicados,'errados'=> $errados, 'cerrados'=>$cerrados,'agendados'=> $agendados, 'cagendados'=>$cagendados, 'ninteresados'=> $ninteresados , 'cninteresados'=> $cninteresados,'pendientes'=> $pendiente,'cpendientes'=>$cpendiente]);
			break;

			case 4:
			if (Request::input('fecha')) {

				// dd(Request::input('fecha'));
				$fecha1 = \DateTime::createFromFormat('d/m/Y H:i',Request::input('fecha').' 00:00');
				$fecha2 = \DateTime::createFromFormat('d/m/Y H:i',Request::input('fecha').' 23:59');
				$agendados = Lead::where('estado','=',4)->where('ejecutivo','=', Auth::user()->id)->whereBetween('created_at', [$fecha1, $fecha2])->get();
				$cagendados = Lead::where('estado','=',4)->where('ejecutivo','=', Auth::user()->id)->whereBetween('created_at', [$fecha1, $fecha2])->count();
				$matriculados = Lead::where('estado','=',5)->where('ejecutivo','=', Auth::user()->id)->whereBetween('updated_at', [$fecha1, $fecha2])->get();
				$cmatriculados = Lead::where('estado','=',5)->where('ejecutivo','=', Auth::user()->id)->whereBetween('updated_at', [$fecha1, $fecha2])->count();
				$nmatriculados = Lead::where('estado','=',6)->where('ejecutivo','=', Auth::user()->id)->whereBetween('created_at', [$fecha1, $fecha2])->get();
				$cnmatriculados = Lead::where('estado','=',6)->where('ejecutivo','=', Auth::user()->id)->whereBetween('created_at', [$fecha1, $fecha2])->count();

			}else
			if (Request::input('fecha1') && Request::input('fecha2')) {
				$fecha1 = \DateTime::createFromFormat('d/m/Y H:i',Request::input('fecha1').' 00:00');
				$fecha2 = \DateTime::createFromFormat('d/m/Y H:i',Request::input('fecha2').' 23:59');
				$agendados = Lead::where('estado','=',4)->where('ejecutivo','=', Auth::user()->id)->whereBetween('created_at', [$fecha1, $fecha2])->get();
				$cagendados = Lead::where('estado','=',4)->where('ejecutivo','=', Auth::user()->id)->whereBetween('created_at', [$fecha1, $fecha2])->count();
				$matriculados = Lead::where('estado','=',5)->where('ejecutivo','=', Auth::user()->id)->whereBetween('updated_at', [$fecha1, $fecha2])->get();
				$cmatriculados = Lead::where('estado','=',5)->where('ejecutivo','=', Auth::user()->id)->whereBetween('updated_at', [$fecha1, $fecha2])->count();
				$nmatriculados = Lead::where('estado','=',6)->where('ejecutivo','=', Auth::user()->id)->whereBetween('created_at', [$fecha1, $fecha2])->get();
				$cnmatriculados = Lead::where('estado','=',6)->where('ejecutivo','=', Auth::user()->id)->whereBetween('created_at', [$fecha1, $fecha2])->count();
			}else{
				$agendados = Lead::where('estado','=',4)->where('ejecutivo','=', Auth::user()->id)->get();
				$cagendados = Lead::where('estado','=',4)->where('ejecutivo','=', Auth::user()->id)->count();
				$matriculados = Lead::where('estado','=',5)->where('ejecutivo','=', Auth::user()->id)->get();
				$cmatriculados = Lead::where('estado','=',5)->where('ejecutivo','=', Auth::user()->id)->count();
				$nmatriculados = Lead::where('estado','=',6)->where('ejecutivo','=', Auth::user()->id)->get();
				$cnmatriculados = Lead::where('estado','=',6)->where('ejecutivo','=', Auth::user()->id)->count();

			}
			
			return view('crmejecutivo')->with(['agendados'=> $agendados, 'cagendados'=>$cagendados,'matriculados'=> $matriculados, 'cmatriculados'=>$cmatriculados,'nmatriculados'=> $nmatriculados, 'cnmatriculados'=>$cnmatriculados]);
			break;

			break;


			default:
				# code...
			break;
		}
		
	}

	public function cambiarEstado($identificador){
		$lead = Lead::find($identificador);
		$lead->estado = Request::input('estado');
		$historial = new Historial;
		$historial->id_user = Auth::user()->id;
		$historial->id_lead = $identificador;
		$historial->comentario = Request::input('comentario');
		$historial->save();
		if (Request::input('usuario')) {
			$lead->id_user = Request::input('usuario');
		}
		if (Request::input('matricula')) {
			$lead->matricula = Request::input('matricula');
		}
		if (Request::input('ejecutivo')) {
			$lead->ejecutivo = Request::input('ejecutivo');
		}
		if(Auth::user()->roll == 3){
			$lead->callcenter = Auth::user()->id;
		}
		if (Request::input('agendado')) {
			
			$fecha = \DateTime::createFromFormat('m/d/Y h:i a', Request::input('agendado'));
			$lead->scheduled = $fecha;
		}

		$lead->save();
		return redirect()->back()->with('success', 'Se ha cambiado el estado con éxito!');

	}

	public function called(){

		$lead = Lead::find(Request::input('identificador'));
		$lead->called = 1;
		$lead->save();
		$historial = new Historial;
		$historial->id_user = Auth::user()->id;
		$historial->id_lead = Request::input('identificador');
		$historial->comentario = "Se ha llamado al usuario sin éxito.";
		$historial->save();
		return redirect()->back()->with('success', 'Se ha agregado la llamada al historial');
	}


	public function agendado(){

		$lead = Lead::find(Request::input('id_lead'));
		$fecha = \DateTime::createFromFormat('m/d/Y h:i a', Request::input('agendado'));
		
		$lead->scheduled = $fecha;
		$lead->save();
		return redirect()->back()->with('success', 'Se ha agendado la cita');
	}

	public function pendientesHoy(){

		if (Auth::user()->roll == 1) {
			$hoy = date("Y-m-d"); 
			$datosHoy = Lead::whereIn('estado', [4,9])->whereDate('scheduled','=', $hoy)->orderBy('scheduled')->get();
			$cDatosHoy = Lead::whereIn('estado', [4,9])->whereDate('scheduled','=', $hoy)->orderBy('scheduled')->count();
			return view('hoy')->with('pendientes',$datosHoy)->with('cpendientes',$cDatosHoy);
		}

		if (Auth::user()->roll == 2) {
			$hoy = date("Y-m-d"); 
			$datosHoy = Lead::where('id_user','=',Auth::user()->id)->whereIn('estado', [4,9])->whereDate('scheduled','=', $hoy)->orderBy('scheduled')->get();
			$cDatosHoy = Lead::where('id_user','=',Auth::user()->id)->whereIn('estado', [4,9])->whereDate('scheduled','=', $hoy)->orderBy('scheduled')->count();
			return view('hoy')->with('pendientes',$datosHoy)->with('cpendientes',$cDatosHoy);
		}else{
			return redirect()->back();
		}

	}

	public function filter(){
		return view('filter');
	}
	

	// public function pendienteAgendado(){

	// 	$lead = Lead::find(Request::input('id_lead'));
	// 	$fecha = \DateTime::createFromFormat('m/d/Y h:i a', Request::input('agendado'));
	// 	$lead->scheduled = $fecha;
	// 	$lead->save();

	// }
	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
