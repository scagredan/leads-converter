<?php namespace App\Http\Controllers;

use App\Lead;
use DB;
use App\Reminder;
use Auth;
class HomeController extends Controller {



	/*
	|--------------------------------------------------------------------------
	| Home Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders your application's "dashboard" for users that
	| are authenticated. Of course, you are free to change or remove the
	| controller as you wish. It is just here to get your app started!
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('auth');
	}

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
		$hoy = $ldate = date('Y-m-d H:i:s');
		$d = strtotime("today");
		$start_week = strtotime("last sunday midnight",$d);
		$end_week = strtotime("next saturday",$d);
		$start = date("Y-m-d",$start_week); 
		$end = date("Y-m-d",$end_week);
		$total_leads = Lead::all()->count();
		$pct_ultima_semana = (Lead::whereBetween('created_at', [$start,$end])->count()*100)/$total_leads;
		$pct_ultima_semana =  number_format((float)$pct_ultima_semana, 2, '.', '');
		$mejor_idioma = Lead::select(DB::raw('count(*) as cant, idioma_id'))->groupBy('idioma_id')->orderBy('cant','desc')->get();
		$mejor_sede = Lead::select(DB::raw('count(*) as cant, sede_id'))->groupBy('sede_id')->orderBy('cant','desc')->get();
		$nuevos = Lead::where('estado', '=', '1')->count();
		$agendados = Lead::where('estado', '=', '4')->count();
		$matriculados = Lead::where('estado', '=', '5')->count();
		
		return view('home')->with(['total_leads'=> $total_leads, 'pct_ultima_semana'=> $pct_ultima_semana, 'mejor_idioma'=> $mejor_idioma, 'mejor_sede'=>$mejor_sede, 'nuevos'=>$nuevos,'agendados'=>$agendados,'matriculados'=>$matriculados]);
	}

	public function recordatorios(){
		date_default_timezone_set('America/Bogota');
		$date = date('Y/m/d h:i:s', time());
		switch (Auth::user()->roll) {
			case '1':
				$recordatorios = Reminder::where('fecha','>',$date)->orderBy('created_at','asc');
				break;
			
			default:
				$recordatorios = Reminder::where('id_user','=', Auth::user()->id)->where('fecha','>',$date)->orderBy('created_at','asc')->get();
				break;
		}
		
		return view('recordatorios')->with('recordatorios', $recordatorios);
	}

	public function amarillas(){
		return view('amarillas');
	}

}
