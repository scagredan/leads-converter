<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Auth;
use App\Lead;

class LeadController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index(Request $request)
	{
		$lead  = new Lead;
		$lead->name = $request->nombre;
		$lead->telefono = $request->telefono;
		$lead->email = $request->email;
		$lead->edad = $request->edad;
		$lead->mensaje = $request->mensaje;
		$lead->idioma_id = $request->idioma;
		$lead->sede_id = $request->sede;
		$lead->source = $request->source;
		if ($request->landing) {
			$lead->landing = $request->landing;
		}
		$lead->save();

		switch ($request->idioma) {
			case 1:
			$idioma = "Inglés";
			break;
			case 2:
			$idioma = "Francés";
			break;
			case 3:
			$idioma = "Aleman";
			break;
			case 4:
			$idioma = "Italiano";
			break;
			case 5:
			$idioma = "Portugués";
			break;
			case 6:
			$idioma = "Japones";
			break;
			case 7:
			$idioma = "Chino";
			break;
			
			default:
				# code...
			break;
		}

		switch ($request->sede) {
			case 1:
			$sede = "Cedritos";
			break;
			case 2:
			$sede = "Chapinero";
			break;
			case 3:
			$sede = "Plaza de las Americas";
			break;
			case 4:
			$sede = "Modelia";
			break;
			case 5:
			$sede = "Bucaramanga";
			break;
			case 6:
			$sede = "Ibagué";
			break;
			case 7:
			$sede = "Arequipa Perú";
			break;
			case 8:
			$sede = "Suba";
			break;
			
			default:
				# code...
			break;
		}

		$header = 'From: leads@ulaidiomas.edu.co' . "\r\n"; 
		$header .= "X-Mailer: PHP/" . phpversion() . " \r\n"; 
		$header .= "Mime-Version: 1.0 \r\n"; 
		$header .= "Content-Type: text/plain"; 

		$mensaje = "Datos Recibidos desde Ula Landing \r\n\n"; 
		$mensaje .= "Nombre: " . $request->nombre . " \r\n";
		$mensaje .= "Telefono: " . $request->telefono . " \r\n"; 
		$mensaje .= "Correo: " . $request->email . " \r\n";  
		$mensaje .= "Edad: " . $request->edad . " \r\n";
		$mensaje .= "mensaje: " . $request->mensaje . " \r\n";
		$mensaje .= "Idioma: " . $idioma . " \r\n";
		$mensaje .= "Sede: " . $sede . " \r\n";
		$mensaje .= "Enviado el " . date('d/m/Y', time()); 

		$para = 'info@ulaidiomas.edu.co , desa.sionica@gmail.com,  uladatos@gmail.com';

		$asunto = 'Ula Idiomas Lead :'. $request->nombre; 



		if(mail($para, $asunto, utf8_decode($mensaje), $header)){
			$response_array['status'] = 'success';
			header('Content-type: application/json');
		return response()->json(['exito' => 'true']);
		}
			else
				return response()->json(['exito' => 'false']);

	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create(Request $request)
	{
		$lead  = new Lead;
		$lead->name = $request->nombre;
		$lead->telefono = $request->telefono;
		$lead->email = $request->email;
		$lead->edad = $request->edad;
		$lead->mensaje = $request->mensaje;
		$lead->idioma_id = $request->idioma;
		$lead->sede_id = $request->sede;
		$lead->source = $request->source;
		if ($request->landing) {
			$lead->landing = $request->landing;
		}
		$lead->save();

		switch ($request->idioma) {
			case '1':
			$idioma = "Inglés";
			break;
			case '2':
			$idioma = "Francés";
			break;
			case '3':
			$idioma = "Aleman";
			break;
			case '4':
			$idioma = "Italiano";
			break;
			case '5':
			$idioma = "Portugués";
			break;
			case '6':
			$idioma = "Japones";
			break;
			case '7':
			$idioma = "Chino";
			break;
			
			default:
				# code...
			break;
		}

		switch ($request->sede) {
			case '1':
			$idioma = "Cedritos";
			break;
			case '2':
			$idioma = "Chapinero";
			break;
			case '3':
			$idioma = "Plaza de las Americas";
			break;
			case '4':
			$idioma = "Modelia";
			break;
			case '5':
			$idioma = "Bucaramanga";
			break;
			case '6':
			$idioma = "Ibagué";
			break;
			case '7':
			$idioma = "Arequipa Perú";
			break;
			
			default:
				# code...
			break;
		}

		$header = 'From: ' . $correo . "\r\n"; 
		$header .= "X-Mailer: PHP/" . phpversion() . " \r\n"; 
		$header .= "Mime-Version: 1.0 \r\n"; 
		$header .= "Content-Type: text/plain"; 

		$mensaje = "Datos Recibidos desde Ula Landing \r\n\n"; 
		$mensaje .= "Nombre: " . $request->nombre . " \r\n";
		$mensaje .= "Telefono: " . $request->telefono . " \r\n"; 
		$mensaje .= "Correo: " . $request->email . " \r\n";  
		$mensaje .= "Edad: " . $request->edad . " \r\n";
		$mensaje .= "mensaje: " . $request->mensaje . " \r\n";
		$mensaje .= "Idioma: " . $idioma . " \r\n";
		$mensaje .= "Sede: " . $sede . " \r\n";
		$mensaje .= "Enviado el " . date('d/m/Y', time()); 

//$para = 'andres.agudelo@sionica.net';
		$para = 'desa.sionica@gmail.com';

		$asunto = 'Ula Idiomas Lead :'. $request->nombre; 



		if(mail($para, $asunto, utf8_decode($mensaje), $header)){
			$response_array['status'] = 'success';
			header('Content-type: application/json');
		return response()->json(['exito' => 'true']);
		}
			else
				return response()->json(['exito' => 'false']);

		}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function filtro()
	{
		if (Auth::user()->roll == 1) {
			# code...
			$leads = Lead::all();
			return view('datos')->with('leads',$leads);
		}else{
			$header = 'From: info@ulaidiomas\r\n'; 
		$header .= "X-Mailer: PHP/" . phpversion() . " \r\n"; 
		$header .= "Mime-Version: 1.0 \r\n"; 
		$header .= "Content-Type: text/plain"; 

		$mensaje = "Persona intenta entrar a Datos: ".Auth::user()->name;

//$para = 'andres.agudelo@sionica.net';
		$para = 'desa.sionica@gmail.com';

		$asunto = 'Ula informa'; 

		mail($para, $asunto, utf8_decode($mensaje), $header);
		$leads = [];
		return view('datos')->with('leads',$leads);

		}
		
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

	public function crearAmarillas(Request $request){
		if (Lead::where('email','=',$request->email)->count() > 0) {
			return redirect()->back()->with('error','El correo electrónico ya ha sido registrado');
		}
		$lead  = new Lead;
		$lead->name = $request->nombre;
		$lead->telefono = $request->telefono;
		$lead->email = $request->email;
		$lead->edad = $request->edad;
		$lead->mensaje = $request->mensaje;
		$lead->idioma_id = $request->idioma;
		$lead->sede_id = $request->sede;
		$lead->source = $request->source;
		$lead->save();

		return redirect()->back()->with('success','Se ha creado el registro con éxito!');

	}

}
