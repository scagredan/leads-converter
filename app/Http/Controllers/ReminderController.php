<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Request;
use App\Reminder;
use Auth;

class ReminderController extends Controller {
public function __construct()
	{
		$this->middleware('auth');
	}
	public function agregar($identificador){
		$reminder = new Reminder();
		$fecha = \DateTime::createFromFormat('m/d/Y h:i a', Request::input('fecha'));
	
		$reminder->fecha = $fecha;
		$reminder->comentario = Request::input('comentario');
		$reminder->id_lead = $identificador;
		$reminder->id_user = Auth::getUser()->id;
		$reminder->save();
		return redirect()->back()->with('success','Se ha creado el recordatorio con éxito!');;
	}

}
