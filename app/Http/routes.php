<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'HomeController@index');

Route::get('home', 'HomeController@index');

Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);

Route::resource('lead', 'LeadController');
Route::resource('usuarios', 'UsersController');
Route::get('datos','LeadController@filtro');
Route::get('crm','CrmController@index');
Route::get('amarillas', 'HomeController@amarillas');
Route::get('crearamarillas','LeadController@crearAmarillas');
Route::get('recordatorios','HomeController@recordatorios');
Route::post('cambiar-estado/{identificador}','CrmController@cambiarEstado');
Route::post('agregar-recordatorio/{identificador}','ReminderController@agregar');
Route::post('called','CrmController@called');
Route::post('agregar-cita/{identificador}', 'CrmController@agendado');
Route::get('pendientes-hoy', 'CrmController@pendientesHoy');
Route::get('filtro', 'CrmController@filter');

