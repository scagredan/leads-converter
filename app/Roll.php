<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Roll extends Model {

	protected $table = "rolls";

	public function getUsers()
    {
        return $this->hasMany('App\User', 'roll');
    }

}
