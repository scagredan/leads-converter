@extends('app')
@section('css')

@endsection
@section('content')
<!-- page content -->
<div class="right_col" role="main">
  <div class="">
    <div class="page-title">
      <div class="title_left">
        <h3>Registrar datos</h3>
      </div>


    </div>
    <div class="clearfix"></div>
    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
          <div class="x_title">
            <h2>Ingresa el registro <small>Verifica los datos</small></h2>
            <ul class="nav navbar-right panel_toolbox">
              <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
              </li>


            </ul>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
            <br />
            <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left" method="get" action="/crearamarillas">
             

             <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Nombre <span class="required">*</span>
              </label>
              <div class="col-md-6 col-sm-6 col-xs-12">
                <input type="text" id="first-name" name="nombre" required="required" class="form-control col-md-7 col-xs-12">
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Email <span class="required">*</span>
              </label>
              <div class="col-md-6 col-sm-6 col-xs-12">
                <input type="text" id="last-name" name="email" required="required" class="form-control col-md-7 col-xs-12">
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Telefono <span class="required">*</span>
              </label>
              <div class="col-md-6 col-sm-6 col-xs-12">
                <input type="text" id="last-name" name="telefono" required="required" class="form-control col-md-7 col-xs-12">
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Edad <span class="required">*</span>
              </label>
              <div class="col-md-6 col-sm-6 col-xs-12">
                <input type="text" id="last-name" name="edad" required="required" class="form-control col-md-7 col-xs-12">
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Idioma <span class="required">*</span>
              </label>
              <div class="col-md-6 col-sm-6 col-xs-12">
                <select type="text" id="last-name" name="idioma" required="required" class="form-control col-md-7 col-xs-12">
                  <option value="" disabled="" selected="">Escoge el idioma</option>
                  <option value="1">Inglés</option>
                  <option value="2">Francés</option>
                  <option value="3">Alemán</option>
                  <option value="4">Italiano</option>
                  <option value="5">Portugués</option>
                  <option value="6">Japonés</option>
                  <!-- <option value="7">Chino</option> -->
                </select>
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Sede <span class="required">*</span>
              </label>
              <div class="col-md-6 col-sm-6 col-xs-12">
                <select type="text" id="last-name" name="sede" required="required" class="form-control col-md-7 col-xs-12">
                  <option value="" disabled="" selected="">Escoge la sede</option>
                  <option value="1">Cedritos</option>
                  <option value="2">Chapinero</option>
                  <option value="3">Plaza de las americas</option>
                  <option value="4">Modelia</option>
                  <option value="5">Bucaramanga</option>
                  <option value="8">Suba</option>
                  <!-- <option value="6">Ibagué</option> -->
                  <option value="7">Arequipa Perú</option>
                </select>
            </div>
          </div>
          <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Origen <span class="required">*</span>
              </label>
              <div class="col-md-6 col-sm-6 col-xs-12">
                <select type="text" id="last-name" name="source" required="required" class="form-control col-md-7 col-xs-12">
                  <option value="" disabled="" selected="">Escoge el Origen</option>
                  <option value="Facebook chat">Facebook chat</option>
                  <option value="Facebook comments">Facebook comments</option>
                  <option value="Instagram">Instagram</option>
                  <option value="Whatsapp">Whatsapp</option>
                  <option value="Amarillas">Amarillas</option>
                  <option value="Referido">Referido</option>
s                </select>
            </div>
          </div>



          <div class="ln_solid"></div>
          <div class="form-group">
            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
              <button class="btn btn-primary" type="button">Cancelar</button>
              <button class="btn btn-primary" type="reset">Limpiar</button>
              <button type="submit" class="btn btn-success">Enviar</button>
            </div>
          </div>

        </form>
      </div>
    </div>
  </div>
</div>



</div>
</div>
<!-- /page content -->
@endsection
@section('css')
@endsection