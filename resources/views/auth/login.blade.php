@extends('template-login')

@section('content')
<body class="login">
	@if (count($errors) > 0)
	<div class="alert alert-danger">
		<strong>Whoops!</strong> There were some problems with your input.<br><br>
		<ul>
			@foreach ($errors->all() as $error)
			<li>{{ $error }}</li>
			@endforeach
		</ul>
	</div>
	@endif
	<div>
		<a class="hiddenanchor" id="signup"></a>
		<a class="hiddenanchor" id="signin"></a>

		<div class="login_wrapper">
			<div class="animate form login_form">
				<section class="login_content">
					<form role="form" method="POST" action="{{ url('/auth/login') }}">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
						<h1>Iniciar Sesión</h1>
						<div>
							<input name="email" type="text" class="form-control" placeholder="Correo electrónico" required="" value="{{ old('email') }}"/>
						</div>
						<div>
							<input name="password" type="password" class="form-control" placeholder="Contraseña" required="" value="{{ old('email') }}" />
						</div>
						<div>
							<div class="checkbox">
								<label>
									<input type="checkbox" name="remember"> Remember Me
								</label>
							</div>
						</div>
						<div>
                <button type="submit" class="btn btn-default submit" ">Iniciar Sesión</button>
              </div>
					

						<div class="clearfix"></div>

						<div class="separator">
							

							<div class="clearfix"></div>
							<br />

							<div>
								<img src="/images/ula-color.png"  alt="" style="width: 150px; margin-bottom: 20px;">
								<p>©2017 Leads Converter || Todos los derechos reservados</p>
							</div>
						</div>
					</form>
				</section>
			</div>

			
		</div>
	</div>
</body>
@endsection
