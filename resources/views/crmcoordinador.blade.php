<?php 	use App\Historial;
use App\User; ?>
@extends('app')
@section('css')
<link href="/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
<link href="/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
<link href="/vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
<link href="/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
<link href="/vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">
<!-- bootstrap-daterangepicker -->
<link href="/vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
<style>
.right_col{
	min-height: auto!important;
}
</style>
@endsection
@section('scripts')
<!-- Datatables -->
<script src="/vendors/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<script src="/vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
<script src="/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
<script src="/vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
<script src="/vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
<script src="/vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
<script src="/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
<script src="/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
<script src="/vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
<script src="/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
<script src="/vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
<script src="/vendors/jszip/dist/jszip.min.js"></script>
<script src="/vendors/pdfmake/build/pdfmake.min.js"></script>
<script src="/vendors/pdfmake/build/vfs_fonts.js"></script>
<script src="/vendors/nprogress/nprogress.js"></script>
<!-- bootstrap-daterangepicker -->
<script src="/vendors/moment/min/moment.min.js"></script>
<script src="/vendors/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- bootstrap-datetimepicker -->    
<script src="/vendors/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>

<script>
	function changeModal(identificador){
		
		$('#formModal').attr('action', '/cambiar-estado/'+identificador);
	}
	function changeRecordatorio(identificador){
		$('#CambiarEstadoModal').attr('action', '/agregar-recordatorio/'+identificador);
		
	}
	function changeCita(identificador){
		$('#id_lead').val(identificador);
		$('#CambiarCitaModal').attr('action', '/agregar-cita/'+identificador);
		
	}
	$('#estado-slc').change(function(){
		if ($('#estado-slc').val()==5) {
			$('#cuadro-matricula').show();
		}else{
			$('#cuadro-matricula').hide();
		}
	});
	$('#estado-slc').change(function(){
		if ($('#estado-slc').val()==4) {
			$('#cuadro-ejecutivo').show();
		}else{
			$('#cuadro-ejecutivo').hide();
		}
	});
	$('#myDatepicker3').datetimepicker();
	$('#myDatepicker4').datetimepicker();
	$('#myDatepicker5').datetimepicker();
	$('#myDatepicker9').datetimepicker();



</script>
<script>
	$(document).ready(function() {
		$('#nuevosprospectos').DataTable( {
			"order": [[ 6, "desc" ]]
		} );
		$('#errados').DataTable( {
			"order": [[ 6, "desc" ]]
		} );
		$('#duplicados').DataTable( {
			"order": [[ 6, "desc" ]]
		} );
		$('#agendados').DataTable( {
			"order": [[ 6, "desc" ]]
		} );
		$('#finalizados').DataTable( {
			"order": [[ 6, "desc" ]]
		} );
		$('#pendientesc').DataTable( {
			"order": [[ 6, "desc" ]]
		} );
		$('#nmatriculados').DataTable( {
			"order": [[ 6, "desc" ]]
		} );
	} );	
</script>
@endsection
@section('content')
<div class="right_col" role="main">
	<div class="">
		<div class="page-title">
			<div class="title_left">
				<h3>CRM</h3>
			</div>


		</div>

		<div class="clearfix"></div>

		<div class="">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="x_panel">
					<div class="x_title">
						<h2> Gestión de nuevos leads <small></small></h2>

						<div class="clearfix"></div>
					</div>
					<div class="x_content">


						<div class="" role="tabpanel" data-example-id="togglable-tabs">
							<ul id="myTab" class="nav nav-tabs nav-justified" role="tablist">								
								<li role="presentation" class="active"><a href="#tab_content4" role="tab" id="profile-tab3" data-toggle="tab" aria-expanded="false">Agendados ({{$cagendados}})</a>
								</li>
								<li role="presentation" class=""><a href="#tab_contentc" role="tab" id="profile-tab6" data-toggle="tab" aria-expanded="false">Pendientes ({{$cpendientesc}})</a>
								</li>
								<li role="presentation" class=""><a href="#tab_content5" role="tab" id="profile-tab4" data-toggle="tab" aria-expanded="false">Matriculados ({{$cfinalizados}})</a>
								</li>
								<li role="presentation" class=""><a href="#tab_content6" role="tab" id="profile-tab5" data-toggle="tab" aria-expanded="false">No Matriculados ({{$cnfinalizados}})</a>
								</li>
							</ul>
							<div id="myTabContent" class="tab-content">

								<div role="tabpanel" class="tab-pane fade in active" id="tab_content4" aria-labelledby="profile-tab">
									<div class="x_title">
										<h2>Lista</h2>
										<ul class="nav navbar-right panel_toolbox">
											<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
											</li>
										</ul>
										<div class="clearfix"></div>
									</div>
									<div class="x_content">                    
										<table id="agendados" class="table table-striped table-bordered">
											<thead>
												<tr>
													<th>Nombre</th>
													<th>Email</th>
													<th>Teléfono</th>
													<th>Edad</th>
													<th>Idioma</th>
													<th>Sede</th>
													<th>Fecha</th>
													<th>Tipo</th>
													<th>Ejecutivo</th>
													<th>Acciones</th>
												</tr>
											</thead>
											<tbody>
												@foreach($agendados as $lead)
												<tr>
													<td>{{$lead->name}}</td>
													<td>{{$lead->email}}</td>
													<td>{{$lead->telefono}}</td>
													<td>{{$lead->edad}}</td>
													<td>
														<?php 
														switch ($lead->idioma_id) {
															case 1:
															echo "Inglés";
															break;
															case 2:
															echo "Francés";
															break;
															case 3:
															echo "Alemán";
															break;
															case 4:
															echo "Italiano";
															break;
															case 5:
															echo "Portugués";
															break;
															case 6:
															echo "Japonés";
															break;
															case 7:
															echo "Chino";
															break;
															
															default:
															
															break;
														}
														?>
													</td>
													<td>
														<?php 
														switch ($lead->sede_id) {
															case 1:
															echo "Cedritos";
															break;
															case 2:
															echo "Chapinero";
															break;
															case 3:
															echo "Plaza de las Americas";
															break;
															case 4:
															echo "Modelia";
															break;
															case 5:
															echo "Bucaramanga";
															break;
															case 6:
															echo "Ibague";
															break;
															case 7:
															echo "Arequipa Perú";
															break;
															case 8:
															echo "Suba";
															break;
															case 9:
															echo "Universidad Nacional";
															break;
															default:
										# code...
															break;
														}
														?>
													</td>

													
													<td>{{$lead->updated_at}}</td>
													@if($lead->landing == 1)
													<td>Adultos</td>
													@elseif($lead->landing == 2)
													<td>Niños</td>
													@else
													<td>Sin Definir</td>
													@endif
													@if($lead->ejecutivo)
													<td>{{User::find($lead->ejecutivo)->name}}</td>
													@else
													<td></td>
													@endif
													<td>
														<a href="#" class="btn btn-info btn-xs" data-toggle="modal" data-target="#cambiar_estado_modal" onclick="changeModal('{{$lead->id}}')"><i class="fa fa-pencil"></i> Cambiar de estado </a>
														<a href="#" class="btn btn-info btn-xs" data-toggle="modal" data-target="#agregar_recordatorio_modal" onclick="changeRecordatorio('{{$lead->id}}')"><i class="fa fa-calendar-plus-o"></i> Agregar recordatorio </a>
														<a href="#" class="btn btn-info btn-xs" data-toggle="modal" data-target="#historialmodal{{$lead->id}}"><i class="fa fa-calendar-plus-o"></i> Ver Historial </a>
														<a href="#" class="btn btn-info btn-xs" data-toggle="modal" data-target="#agregar_cita_modal" onclick="changeCita('{{$lead->id}}')"><i class="fa fa-calendar-plus-o"></i> Agregar cita </a>
														
													</td>
												</tr>
												@endforeach
											</tbody>
										</table>
									</div>
								</div>
								

								<div role="tabpanel" class="tab-pane fade" id="tab_contentc" aria-labelledby="profile-tab">
									<div class="x_title">
										<h2>Lista</h2>
										<ul class="nav navbar-right panel_toolbox">
											<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
											</li>
										</ul>
										<div class="clearfix"></div>
									</div>
									<div class="x_content">                    
										<table id="pendientesc" class="table table-striped table-bordered">
											<thead>
												<tr>
													<th>Nombre</th>
													<th>Email</th>
													<th>Teléfono</th>
													<th>Edad</th>
													<th>Idioma</th>
													<th>Sede</th>
													<th>Fecha</th>
													<th>Tipo</th>
													<th>Ejecutivo</th>
													<th>Acciones</th>
												</tr>
											</thead>
											<tbody>
												@foreach($pendientesc as $lead)
												<tr>
													<td>{{$lead->name}}</td>
													<td>{{$lead->email}}</td>
													<td>{{$lead->telefono}}</td>
													<td>{{$lead->edad}}</td>
													<td>
														<?php 
														switch ($lead->idioma_id) {
															case 1:
															echo "Inglés";
															break;
															case 2:
															echo "Francés";
															break;
															case 3:
															echo "Alemán";
															break;
															case 4:
															echo "Italiano";
															break;
															case 5:
															echo "Portugués";
															break;
															case 6:
															echo "Japonés";
															break;
															case 7:
															echo "Chino";
															break;
															
															default:
															
															break;
														}
														?>
													</td>
													<td>
														<?php 
														switch ($lead->sede_id) {
															case 1:
															echo "Cedritos";
															break;
															case 2:
															echo "Chapinero";
															break;
															case 3:
															echo "Plaza de las Americas";
															break;
															case 4:
															echo "Modelia";
															break;
															case 5:
															echo "Bucaramanga";
															break;
															case 6:
															echo "Ibague";
															break;
															case 7:
															echo "Arequipa Perú";
															break;
															case 8:
															echo "Suba";
															break;
															case 9:
															echo "Universidad Nacional";
															break;
															default:
										# code...
															break;
														}
														?>
													</td>

													
													<td>{{$lead->updated_at}}</td>
													@if($lead->landing == 1)
													<td>Adultos</td>
													@elseif($lead->landing == 2)
													<td>Niños</td>
													@else
													<td>Sin Definir</td>
													@endif
													@if($lead->ejecutivo)
													<td>{{User::find($lead->ejecutivo)->name}}</td>
													@else
													<td></td>
													@endif
													<td>
														<a href="#" class="btn btn-info btn-xs" data-toggle="modal" data-target="#cambiar_estado_modal" onclick="changeModal('{{$lead->id}}')"><i class="fa fa-pencil"></i> Cambiar de estado </a>
														<a href="#" class="btn btn-info btn-xs" data-toggle="modal" data-target="#agregar_recordatorio_modal" onclick="changeRecordatorio('{{$lead->id}}')"><i class="fa fa-calendar-plus-o"></i> Agregar recordatorio </a>
														<a href="#" class="btn btn-info btn-xs" data-toggle="modal" data-target="#historialmodal{{$lead->id}}"><i class="fa fa-calendar-plus-o"></i> Ver Historial </a>
														<a href="#" class="btn btn-info btn-xs" data-toggle="modal" data-target="#agregar_cita_modal" onclick="changeCita('{{$lead->id}}')"><i class="fa fa-calendar-plus-o"></i> Agregar cita </a>
														
													</td>
												</tr>
												@endforeach
											</tbody>
										</table>
									</div>
								</div>
								<div role="tabpanel" class="tab-pane fade" id="tab_content5" aria-labelledby="profile-tab">
									<div class="x_title">
										<h2>Lista</h2>
										<ul class="nav navbar-right panel_toolbox">
											<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
											</li>
										</ul>
										<div class="clearfix"></div>
									</div>
									<div class="x_content">                    
										<table id="finalizados" class="table table-striped table-bordered">
											<thead>
												<tr>
													<th>Nombre</th>
													<th>Email</th>
													<th>Teléfono</th>
													<th>Edad</th>
													<th>Idioma</th>
													<th>Sede</th>
													<th>Fecha</th>
													<th>Tipo</th>
													<th>Ejecutivo</th>
													<th>No Matricula</th>
													<th>Acciones</th>
												</tr>
											</thead>
											<tbody>
												@foreach($finalizados as $lead)
												<tr>
													<td>{{$lead->name}}</td>
													<td>{{$lead->email}}</td>
													<td>{{$lead->telefono}}</td>
													<td>{{$lead->edad}}</td>
													<td>
														<?php 
														switch ($lead->idioma_id) {
															case 1:
															echo "Inglés";
															break;
															case 2:
															echo "Francés";
															break;
															case 3:
															echo "Alemán";
															break;
															case 4:
															echo "Italiano";
															break;
															case 5:
															echo "Portugués";
															break;
															case 6:
															echo "Japonés";
															break;
															case 7:
															echo "Chino";
															break;
															
															default:
															
															break;
														}
														?>
													</td>
													<td>
														<?php 
														switch ($lead->sede_id) {
															case 1:
															echo "Cedritos";
															break;
															case 2:
															echo "Chapinero";
															break;
															case 3:
															echo "Plaza de las Americas";
															break;
															case 4:
															echo "Modelia";
															break;
															case 5:
															echo "Bucaramanga";
															break;
															case 6:
															echo "Ibague";
															break;
															case 7:
															echo "Arequipa Perú";
															break;
															case 8:
															echo "Suba";
															break;
															case 9:
															echo "Universidad Nacional";
															break;
															
															default:
										# code...
															break;
														}
														?>
													</td>

													
													<td>{{$lead->updated_at}}</td>
													@if($lead->landing == 1)
													<td>Adultos</td>
													@elseif($lead->landing == 2)
													<td>Niños</td>
													@else
													<td>Sin Definir</td>
													@endif
													@if($lead->ejecutivo)
													<td>{{User::find($lead->ejecutivo)->name}}</td>
													@else
													<td></td>
													@endif
													<td>{{$lead->matricula}}</td>
													<td>
														<a href="#" class="btn btn-info btn-xs" data-toggle="modal" data-target="#cambiar_estado_modal" onclick="changeModal('{{$lead->id}}')"><i class="fa fa-pencil"></i> Cambiar de estado </a>
														<a href="#" class="btn btn-info btn-xs" data-toggle="modal" data-target="#agregar_recordatorio_modal" onclick="changeRecordatorio('{{$lead->id}}')"><i class="fa fa-calendar-plus-o"></i> Agregar recordatorio </a>
														<a href="#" class="btn btn-info btn-xs" data-toggle="modal" data-target="#historialmodal{{$lead->id}}"><i class="fa fa-calendar-plus-o"></i> Ver Historial </a>
														
													</td>
												</tr>
												@endforeach
											</tbody>
										</table>
									</div>
								</div>
								<!-- NO FINALIZAIDADO -->
								<div role="tabpanel" class="tab-pane fade" id="tab_content6" aria-labelledby="profile-tab">
									<div class="x_title">
										<h2>Lista</h2>
										<ul class="nav navbar-right panel_toolbox">
											<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
											</li>
										</ul>
										<div class="clearfix"></div>
									</div>
									<div class="x_content">                    
										<table id="nmatriculados" class="table table-striped table-bordered">
											<thead>
												<tr>
													<th>Nombre</th>
													<th>Email</th>
													<th>Teléfono</th>
													<th>Edad</th>
													<th>Idioma</th>
													<th>Sede</th>
													<th>Fecha</th>
													<th>Tipo</th>
													<th>Ejecutivo</th>
													<th>Acciones</th>
												</tr>
											</thead>
											<tbody>
												@foreach($nfinalizados as $lead)
												<tr>
													<td>{{$lead->name}}</td>
													<td>{{$lead->email}}</td>
													<td>{{$lead->telefono}}</td>
													<td>{{$lead->edad}}</td>
													<td>
														<?php 
														switch ($lead->idioma_id) {
															case 1:
															echo "Inglés";
															break;
															case 2:
															echo "Francés";
															break;
															case 3:
															echo "Alemán";
															break;
															case 4:
															echo "Italiano";
															break;
															case 5:
															echo "Portugués";
															break;
															case 6:
															echo "Japonés";
															break;
															case 7:
															echo "Chino";
															break;
															case 8:
															echo "Suba";
															break;
															case 9:
															echo "Universidad Nacional";
															break;
															
															default:
															
															break;
														}
														?>
													</td>
													<td>
														<?php 
														switch ($lead->sede_id) {
															case 1:
															echo "Cedritos";
															break;
															case 2:
															echo "Chapinero";
															break;
															case 3:
															echo "Plaza de las Americas";
															break;
															case 4:
															echo "Modelia";
															break;
															case 5:
															echo "Bucaramanga";
															break;
															case 6:
															echo "Ibague";
															break;
															case 7:
															echo "Arequipa Perú";
															break;
															case 8:
															echo "Suba";
															break;
															case 9:
															echo "Universidad Nacional";
															break;
															
															default:
										# code...
															break;
														}
														?>
													</td>

													
													<td>{{$lead->updated_at}}</td>
													@if($lead->landing == 1)
													<td>Adultos</td>
													@elseif($lead->landing == 2)
													<td>Niños</td>
													@else
													<td>Sin Definir</td>
													@endif
													@if($lead->ejecutivo)
													<td>{{User::find($lead->ejecutivo)->name}}</td>
													@else
													<td></td>
													@endif
													<td>
														
														<a href="#" class="btn btn-info btn-xs" data-toggle="modal" data-target="#agregar_recordatorio_modal" onclick="changeRecordatorio('{{$lead->id}}')"><i class="fa fa-calendar-plus-o"></i> Agregar recordatorio </a>
														<a href="#" class="btn btn-info btn-xs" data-toggle="modal" data-target="#historialmodal{{$lead->id}}"><i class="fa fa-calendar-plus-o"></i> Ver Historial </a>
														<form action="/cambiar-estado/{{$lead->id}}" method="post">
															<input type="hidden" id="id_recordatorio" name="identificador_cambiar_estado">
															<input type="hidden" name="_token" value="{{ csrf_token() }}">
															<input type="hidden" name="estado" value="9">
															<input type="hidden" name="comentario" value="Recuperado de No Matriculados">
															<button type="submit" class="btn btn-info btn-xs"><i class="fa fa-check"></i> Pendiente</button>

															
														</form>
														
													</td>
												</tr>
												@endforeach
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>

					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div id="cambiar_estado_modal" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">

			<form id="formModal" data-parsley-validate class="form-horizontal form-label-left" method="post">
				<input type="hidden" name="_token" value="{{ csrf_token() }}">

				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
					</button>
					<h4 class="modal-title" id="myModalLabel">Cambiar de estado</h4>
				</div>
				<div class="modal-body">
					<div class="x_panel">
						<div class="x_content">
							<br />
							<div class="form-group">
								<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Nuevo estado <span class="required">*</span>
								</label>
								<div class="col-md-6 col-sm-6 col-xs-12">
									<select name="estado" id="estado-slc" required="" class="form-control col-md-7 col-xs-12" name="estado">
										<option value="" disabled="" selected="">Selecciona el nuevo estado</option>
										<option value="4">Agendado (Selecciona Ejecutivo)</option>
										<option value="5">Matriculados</option>
										<option value="6">No Matriculados</option>
										<option value="9">Pendientes</option>
									</select>
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Comentarios adicionales <span class="">*</span>
								</label>
								<div class="col-md-6 col-sm-6 col-xs-12">
									<textarea class="form-control col-md-7 col-xs-12" name="comentario"></textarea>
								</div>
							</div>
							<div class="form-group" id="cuadro-matricula" style="display: none;">
								<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Número de matricula <span class="required">*</span>
								</label>
								<div class="col-md-6 col-sm-6 col-xs-12">
									<input type="text" class="form-control col-md-7 col-xs-12" name="matricula">
								</div>
							</div>
							<div id="cuadro-ejecutivo" style="display: none;">
								<div class="form-group" >
									<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Ejeutivo a cargo <span class="required">*</span>
									</label>
									<div class="col-md-6 col-sm-6 col-xs-12">
										<select  id="estado-slc" novalidate="" class="form-control col-md-7 col-xs-12" name="ejecutivo">
											<option value="" disabled="" selected="">Selecciona el ejecutivo a cargo</option>

											@foreach(User::where('roll','=','4')->get() as $tempo)
											<option value="{{$tempo->id}}">{{$tempo->name}}</option>
											@endforeach
										</select>
									</div>
									
								</div>
								<div class="form-group">
									<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Fecha y hora (Para la cita)<span class="required">*</span>
									</label>
									<div class="col-md-6 col-sm-6 col-xs-12">
										<div class='input-group date' id='myDatepicker9'>
											<input name="agendado" type='text' class="form-control" />
											<span class="input-group-addon">
												<span class="glyphicon glyphicon-calendar"></span>
											</span>
										</div>
									</div>
								</div>
							</div>
							
						</div>
					</div>				
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
					<button type="submit" class="btn btn-primary">Guardar</button>
				</div>
			</form>
		</div>
	</div>
</div>
<div id="agregar_recordatorio_modal" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">

			<form id="CambiarEstadoModal" data-parsley-validate class="form-horizontal form-label-left" method="post">
				<input type="hidden" id="id_recordatorio" name="identificador_cambiar_estado">
				<input type="hidden" name="_token" value="{{ csrf_token() }}">

				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
					</button>
					<h4 class="modal-title" id="myModalLabel">Agregar recordatorio</h4>
				</div>
				<div class="modal-body">
					<div class="x_panel">
						<div class="x_content">
							<br />
							<div class="form-group">
								<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Fecha y hora <span class="required">*</span>
								</label>
								<div class="col-md-6 col-sm-6 col-xs-12">
									<div class='input-group date' id='myDatepicker3'>
										<input name="fecha" type='text' class="form-control" />
										<span class="input-group-addon">
											<span class="glyphicon glyphicon-calendar"></span>
										</span>
									</div>
								</div>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Comentarios adicionales <span class="required">*</span>
							</label>
							<div class="col-md-6 col-sm-6 col-xs-12">
								<textarea  required="required" class="form-control col-md-7 col-xs-12" name="comentario"></textarea>
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
					<button type="submit" class="btn btn-primary">Guardar</button>
				</div>
			</form>
		</div>
	</div>
</div>
@foreach($agendados as $lead)
<div id="historialmodal{{$lead->id}}" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">		
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
				</button>
				<h4 class="modal-title" id="myModalLabel2">Historial</h4>
			</div>
			<div class="modal-body">
				<?php 	$historiales= Historial::where('id_lead','=', $lead->id)->orderby('updated_at','asc')->get() ?>
				<table class="table">
					<thead>
						<tr>
							<th>#</th>
							<th>Usuario</th>
							<th>Comentario</th>
							<th>Fecha</th>
						</tr>
					</thead>
					<tbody>
						@foreach($historiales as $index => $historial)
						<tr>
							<th scope="row">{{$index}}</th>
							<td>{{User::find($historial->id_user)->name}}</td>
							<td>{{$historial->comentario}}</td>
							<td>{{$historial->updated_at}}</td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Aceptar</button>
			</div>

		</div>
	</div>
</div>
@endforeach
@foreach($finalizados as $lead)
<div id="historialmodal{{$lead->id}}" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">		
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
				</button>
				<h4 class="modal-title" id="myModalLabel2">Historial</h4>
			</div>
			<div class="modal-body">
				<?php 	$historiales= Historial::where('id_lead','=', $lead->id)->orderby('updated_at','asc')->get() ?>
				<table class="table">
					<thead>
						<tr>
							<th>#</th>
							<th>Usuario</th>
							<th>Comentario</th>
							<th>Fecha</th>
						</tr>
					</thead>
					<tbody>
						@foreach($historiales as $index => $historial)
						<tr>
							<th scope="row">{{$index}}</th>
							<td>{{User::find($historial->id_user)->name}}</td>
							<td>{{$historial->comentario}}</td>
							<td>{{$historial->updated_at}}</td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Aceptar</button>
			</div>

		</div>
	</div>
</div>
@endforeach
@foreach($nfinalizados as $lead)
<div id="historialmodal{{$lead->id}}" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">		
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
				</button>
				<h4 class="modal-title" id="myModalLabel2">Historial</h4>
			</div>
			<div class="modal-body">
				<?php 	$historiales= Historial::where('id_lead','=', $lead->id)->orderby('updated_at','asc')->get() ?>
				<table class="table">
					<thead>
						<tr>
							<th>#</th>
							<th>Usuario</th>
							<th>Comentario</th>
							<th>Fecha</th>
						</tr>
					</thead>
					<tbody>
						@foreach($historiales as $index => $historial)
						<tr>
							<th scope="row">{{$index}}</th>
							<td>{{User::find($historial->id_user)->name}}</td>
							<td>{{$historial->comentario}}</td>
							<td>{{$historial->updated_at}}</td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Aceptar</button>
			</div>

		</div>
	</div>
</div>
@endforeach
@foreach($pendientesc as $lead)
<div id="historialmodal{{$lead->id}}" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">		
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
				</button>
				<h4 class="modal-title" id="myModalLabel2">Historial</h4>
			</div>
			<div class="modal-body">
				<?php 	$historiales= Historial::where('id_lead','=', $lead->id)->orderby('updated_at','asc')->get() ?>
				<table class="table">
					<thead>
						<tr>
							<th>#</th>
							<th>Usuario</th>
							<th>Comentario</th>
							<th>Fecha</th>
						</tr>
					</thead>
					<tbody>
						@foreach($historiales as $index => $historial)
						<tr>
							<th scope="row">{{$index}}</th>
							<td>{{User::find($historial->id_user)->name}}</td>
							<td>{{$historial->comentario}}</td>
							<td>{{$historial->updated_at}}</td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Aceptar</button>
			</div>

		</div>
	</div>
</div>
@endforeach

<div id="agregar_cita_modal" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">

			<form id="CambiarCitaModal" data-parsley-validate class="form-horizontal form-label-left" method="post">
				<input type="hidden" name="_token" value="{{ csrf_token() }}">
				<input type="hidden" name="id_lead" id="id_lead">

				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
					</button>
					<h4 class="modal-title" id="myModalLabel">Agregar Cita</h4>
				</div>
				<div class="modal-body">
					<div class="x_panel">
						<div class="x_content">
							<br />
							<div class="form-group">
								<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Fecha y hora <span class="required">*</span>
								</label>
								<div class="col-md-6 col-sm-6 col-xs-12">
									<div class='input-group date' id='myDatepicker4'>
										<input name="agendado" type='text' class="form-control" />
										<span class="input-group-addon">
											<span class="glyphicon glyphicon-calendar"></span>
										</span>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
					<button type="submit" class="btn btn-primary">Guardar</button>
				</div>
			</form>
		</div>
	</div>
</div>

@endsection
