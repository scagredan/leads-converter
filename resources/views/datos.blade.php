@extends('app')
@section('css')
<link href="/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
<link href="/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
<link href="/vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
<link href="/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
<link href="/vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">
<style>
.right_col{
	min-height: auto!important;
}
</style>
@endsection
@section('content')
<!-- page content -->
<div class="right_col" >
	<div class="">
		<div class="page-title">
			<div class="title_left">
				<h3>Datos <small>Módulo para visualización de datos</small></h3>
			</div>
		</div>

		<div class="clearfix"></div>

		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="x_panel">
					<div class="x_title">
						<h2>Lista</h2>
						<ul class="nav navbar-right panel_toolbox">
							<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
							</li>
						</ul>
						<div class="clearfix"></div>
					</div>
					<div class="x_content">                    
						<table id="datatabledatos" class="table table-striped table-bordered">
							<thead>
								<tr>
									<th>Nombre</th>
									<th>Email</th>
									<th>Teléfono</th>
									<th>Edad</th>
									<th>Idioma</th>
									<th>Sede</th>
									<th>Origen</th>
									<th>Fecha</th>
									<th>Tipo</th>
								</tr>
							</thead>
							<tbody>
								@foreach($leads as $lead)
								<tr>
									<td>{{$lead->name}}</td>
									<td>{{$lead->email}}</td>
									<td>{{$lead->telefono}}</td>
									<td>{{$lead->edad}}</td>
									<td>
										<?php 
										switch ($lead->idioma_id) {
											case 1:
											echo "Inglés";
											break;
											case 2:
											echo "Francés";
											break;
											case 3:
											echo "Alemán";
											break;
											case 4:
											echo "Italiano";
											break;
											case 5:
											echo "Portugués";
											break;
											case 6:
											echo "Japonés";
											break;
											case 7:
											echo "Chino";
											break;
											
											default:
											
											break;
										}
										?>
									</td>
									<td>
										<?php 
										switch ($lead->sede_id) {
											case 1:
											echo "Cedritos";
											break;
											case 2:
											echo "Chapinero";
											break;
											case 3:
											echo "Plaza de las Americas";
											break;
											case 4:
											echo "Modelia";
											break;
											case 5:
											echo "Bucaramanga";
											break;
											case 6:
											echo "Ibague";
											break;
											case 7:
											echo "Arequipa Perú";
											break;
											case 8:
											echo "Suba";
											break;
											case 9:
											echo "Universidad Nacional";
											break;
											
											default:

										# code...
											break;
										}
										?>
									</td>

									<td>{{$lead->source}}</td>
									<td>{{$lead->created_at}}</td>
									@if($lead->landing == 1)
									<td>Adultos</td>
									@elseif($lead->landing == 2)
									<td>Niños</td>
									@else
									<td>Sin Definir</td>
									@endif
								</tr>
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>		
@endsection
@section('scripts')
<!-- Datatables -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.5/jszip.js"></script>
<script src="/vendors/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<script src="/vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
<script src="/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
<script src="/vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
<script src="/vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
<script src="/vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
<script src="/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
<script src="/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
<script src="/vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
<script src="/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
<script src="/vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
<script src="/vendors/jszip/dist/jszip.min.js"></script>
<script src="/vendors/pdfmake/build/pdfmake.min.js"></script>
<script src="/vendors/pdfmake/build/vfs_fonts.js"></script>
<script>
	$(document).ready(function() {
		$('#datatabledatos').DataTable( {
			"order": [[ 7, "desc" ]],
			dom: "Bfrtip",
			buttons:   [
			'copyHtml5',
			'excelHtml5',
			'csvHtml5',
			'pdfHtml5'
			]
		} );
	} );
</script>
@endsection