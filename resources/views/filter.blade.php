<?php 	use App\Historial;
use App\User; ?>
@extends('app')
@section('css')
<link href="/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
<link href="/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
<link href="/vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
<link href="/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
<link href="/vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">
<!-- bootstrap-daterangepicker -->
<link href="/vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
<style>
.right_col{
	min-height: auto!important;
}
</style>
@endsection
@section('scripts')
<!-- Datatables -->
<script src="/vendors/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<script src="/vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
<script src="/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
<script src="/vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
<script src="/vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
<script src="/vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
<script src="/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
<script src="/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
<script src="/vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
<script src="/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
<script src="/vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
<script src="/vendors/jszip/dist/jszip.min.js"></script>
<script src="/vendors/pdfmake/build/pdfmake.min.js"></script>
<script src="/vendors/pdfmake/build/vfs_fonts.js"></script>
<script src="/vendors/nprogress/nprogress.js"></script>
<!-- bootstrap-daterangepicker -->
<script src="/vendors/moment/min/moment.min.js"></script>
<script src="/vendors/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- bootstrap-datetimepicker -->    
<script src="/vendors/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>

<script>


	
	$('#myDatepicker4').datetimepicker({
                 format: 'DD/MM/YYYY'
           });
	$('#myDatepicker5').datetimepicker({
                 format: 'DD/MM/YYYY'
           });
	$('#myDatepicker6').datetimepicker({
                 format: 'DD/MM/YYYY'
           });



</script>

@endsection
@section('content')
<div class="right_col" role="main">
	<div class="">
		<div class="page-title">
			<div class="title_left">
				<h3>Filtro</h3>
			</div>


		</div>

		<div class="clearfix"></div>

		<div class="">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="x_panel">
					<div class="x_title">
						<h2> Ordene la información según el rango de fechas que necesita <small></small></h2>

						<div class="clearfix"></div>
					</div>
					<div class="x_content">

						 <div class="card-header">
            <h2 class="card-title">CONSULTAS</h2>
            <small class="card-subtitle">Filtra aquí por fechas tus consultas</small>
        </div>
        <div class="card-block">
            <div class="tab-container">
                <ul class="nav nav-tabs nav-fill" role="tablist">
                    <li class="nav-item active">
                        <a class="nav-link active" data-toggle="tab" href="#fecha" role="tab">Fecha Exacta</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#rango" role="tab">Rango de Fechas</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/crm" >Ver todos</a>
                    </li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="fecha" role="tabpanel">
                        <form action="/crm" id="form-exacto">
                            
                            <div class="form-group  m-b-30">
                                <div class="  ">
                                    <div class="radio m-b-15">
                                        <label>
                                            <i class="input-helper"></i>
                                            Día exacto
                                        </label>
                                    </div>
                                </div>
                               <div class="col-md-6 col-sm-6 col-xs-12">
										<div class='input-group date' >
											<input id='myDatepicker4' name="fecha" type='text' class="form-control" placeholder="Fecha" />
										</div>
									</div>

                                <button type="submit" class="btn btn-danger btn-block btn-gestion">FILTRAR</button>
                            </div>
                        </form>
                    </div>
                    <div class="tab-pane fade" id="rango" role="tabpanel">
                        <form action="/crm" method="get" id="form-rango">
                            
                            <div class="form-group  m-b-30">
                                <div class="  ">
                                    <div class="radio m-b-15">
                                        <label>
                                            <i class="input-helper"></i>
                                            Rango de fechas
                                        </label>
                                    </div>
                                </div>
                                <div id="dv-rango">
                                    <div class="col-md-6 col-sm-6 col-xs-12">
										<div class='input-group date' >
											<input id='myDatepicker5' name="fecha1" type='text' class="form-control" placeholder="Fecha Inicial" />
											
										</div>
									</div>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
										<div class='input-group date' >
											<input id='myDatepicker6' name="fecha2" type='text' class="form-control" placeholder="Fecha Final" />
											
										</div>
									</div>
                                    
                                </div>
                                <button type="submit" class="btn btn-danger btn-block btn-gestion">FILTRAR</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            

        </div>
						

					</div>
				</div>
			</div>
		</div>
	</div>
</div>


@endsection
