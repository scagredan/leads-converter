@extends('app')
@section('css')
<!-- bootstrap-progressbar -->
<link href="/vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet">
<!-- JQVMap -->
<link href="/vendors/jqvmap/dist/jqvmap.min.css" rel="stylesheet"/>
<!-- bootstrap-daterangepicker -->
<link href="/vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
@endsection
@section('content')

<!-- page content -->
<div class="right_col" role="main">
  <!-- top tiles -->
  <div class="row tile_count">
    <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
      <span class="count_top"><i class="fa fa-user"></i> Total Leads</span>
      <div class="count">{{$total_leads}}</div>
      <span class="count_bottom"><i class="green">{{$pct_ultima_semana}}% </i> De la última semana</span>
    </div>
    <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
      <span class="count_top"><i class="fa fa-clock-o"></i> Mejor Idioma</span>
      <?php  $name_mejor_idioma =  $mejor_idioma[0]->cant*100/$total_leads; 
      $name = number_format((float)$name_mejor_idioma, 2, '.', '');
      switch ($mejor_idioma[0]->idioma_id) {

        case 1:
        $idioma = "Inglés";
        break;
        case 2:
        $idioma = "Francés";
        break;
        case 3:
        $idioma = "Aleman";
        break;
        case 4:
        $idioma = "Italiano";
        break;
        case 5:
        $idioma = "Portugués";
        break;
        case 6:
        $idioma = "Japones";
        break;
        case 7:
        $idioma = "Chino";
        break;

        default:
        # code...
        break;
      }

      ?>
      <div class="count">{{$name}}%</div>
      <span class="count_bottom"><i class="green"><i class="fa fa-sort-asc"> </i></i>{{$idioma}}</span>
    </div>
    <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
      <span class="count_top"><i class="fa fa-user"></i> Mejor Sede</span>
      <?php  $name_mejor_sede =  $mejor_sede[0]->cant*100/$total_leads; 
      $name = number_format((float)$name_mejor_sede, 2, '.', '');
      switch ($mejor_sede[0]->sede_id) {

        case 1:
        $sede = "Cedritos";
        break;
        case 2:
        $sede = "Chapinero";
        break;
        case 3:
        $sede = "Plaza de las Americas";
        break;
        case 4:
        $sede = "Modelia";
        break;
        case 5:
        $sede = "Bucaramanga";
        break;
        case 6:
        $sede = "Ibagué";
        break;
        case 7:
        $sede = "Arequipa Perú";
        break;
        case 8:
        $sede = "Suba";
        break;

        default:
        # code...
        break;
      }

      ?>
      <div class="count green">{{$name}}%</div>
      <span class="count_bottom"><i class="green"><i class="fa fa-sort-asc"></i> </i> {{$sede}}</span>
    </div>
    <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
      <span class="count_top"><i class="fa fa-user"></i> Total Datos Nuevos</span>
      <div class="count">{{$nuevos}}</div>
      <span class="count_bottom"></span>
    </div>
    <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
      <span class="count_top"><i class="fa fa-user"></i> Total Agendados</span>
      <div class="count">{{$agendados}}</div>
      <span class="count_bottom"></span>
    </div>
    <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
      <span class="count_top"><i class="fa fa-user"></i> Total Matriculados</span>
      <div class="count">{{$matriculados}}</div>
      <span class="count_bottom"></span>
    </div>
  </div>
  <!-- /top tiles -->


  <br />

  <div class="row">


    <div class="col-md-4 col-sm-4 col-xs-12">
      <div class="x_panel tile ">
        <div class="x_title">
          <h2>Idiomas </h2>
          <ul class="nav navbar-right panel_toolbox">
            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
            </li>

            <li><a class="close-link"><i class="fa fa-close"></i></a>
            </li>
          </ul>
          <div class="clearfix"></div>
        </div>
        <div class="x_content">
          <h4>Cantidad de usuarios por idioma</h4>
          @foreach ($mejor_idioma as $bidioma)
          <div class="widget_summary">
            <div class="w_left w_25">
              <?php  
              $name_mejor_idioma =  $bidioma->cant*100/$total_leads; 
              $name = number_format((float)$name_mejor_idioma, 2, '.', '');
              switch ($bidioma->idioma_id) {

                case 1:
                $idioma = "Inglés";
                break;
                case 2:
                $idioma = "Francés";
                break;
                case 3:
                $idioma = "Aleman";
                break;
                case 4:
                $idioma = "Italiano";
                break;
                case 5:
                $idioma = "Portugués";
                break;
                case 6:
                $idioma = "Japones";
                break;
                case 7:
                $idioma = "Chino";
                break;

                default:
        # code...
                break;
              }
              ?>
              <span>{{$idioma}}</span>
            </div>
            <div class="w_center w_55">
              <div class="progress">
                <div class="progress-bar bg-green" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: {{$name}}%;">
                  <span class="sr-only">{{$name}}% </span>
                </div>
              </div>
            </div>
            <div class="w_right w_20">
              <span>{{$name}}%</span>
            </div>
            <div class="clearfix"></div>
          </div>
          @endforeach

          

        </div>
      </div>
    </div>

    <div class="col-md-4 col-sm-4 col-xs-12">
      <div class="x_panel tile fixed_height_320 overflow_hidden">
        <div class="x_title">
          <h2>Sedes por Usuarios</h2>
          <ul class="nav navbar-right panel_toolbox">
            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
            </li>

          </ul>
          <div class="clearfix"></div>
        </div>

        <div class="x_content">
          <table class="" style="width:100%">
            <tr>
              
              <th>
                <div class="col-lg-7 col-md-7 col-sm-7 col-xs-7">
                  <p class="">Sede</p>
                </div>
                <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5">
                  <p class="">Leads</p>
                </div>
              </th>
            </tr>
            <tr>
              
              <td>
                <table class="tile_info">
                  @foreach($mejor_sede as $bmejor_sede)
                  <?php  $name_mejor_sede =  $bmejor_sede->cant*100/$total_leads; 
                  $name = number_format((float)$name_mejor_sede, 2, '.', '');
                  switch ($bmejor_sede->sede_id) {

                    case 1:
                    $sede = "Cedritos";
                    break;
                    case 2:
                    $sede = "Chapinero";
                    break;
                    case 3:
                    $sede = "Plaza de las Americas";
                    break;
                    case 4:
                    $sede = "Modelia";
                    break;
                    case 5:
                    $sede = "Bucaramanga";
                    break;
                    case 6:
                    $sede = "Ibagué";
                    break;
                    case 7:
                    $sede = "Arequipa Perú";
                    break;
                    case 8:
                    $sede = "Suba";
                    break;

                    default:
        # code...
                    break;
                  }
                  ?>
                  <tr>
                    <td>
                      <p><i class="fa fa-square blue"></i>{{$sede}} </p>
                    </td>
                    <td>{{$name}}%</td>
                  </tr>
                  @endforeach
                </table>
              </td>
            </tr>
          </table>
        </div>
      </div>
    </div>


    <div class="col-md-4 col-sm-4 col-xs-12">
      <div class="x_panel tile fixed_height_320">
        <div class="x_title">
          <h2>Porcentaje de conversión de usuarios</h2>
          <ul class="nav navbar-right panel_toolbox">
            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
            </li>
          </ul>
          <div class="clearfix"></div>
        </div>
        <div class="x_content">
          <div class="dashboard-widget-content">
            <ul class="quick-list">
              <li><i class="fa fa-calendar-o"></i>Todas las sedes
              </li>
            </ul>

            <div class="sidebar-widget">
              <h4>Porcentaje de conversión</h4>
              <canvas width="150" height="80" id="chart_gauge_01" class="" style="width: 160px; height: 100px;"></canvas>
              <div class="goal-wrapper">
                <span id="gauge-text" class="gauge-value pull-left">0</span>
                <span class="gauge-value pull-left">%</span>
                <span id="goal-text" class="goal-value pull-right">100%</span>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

  </div>



</div>
<!-- /page content -->
<!-- /page content -->

@endsection
@section('scripts')
<!-- NProgress -->
<script src="/vendors/nprogress/nprogress.js"></script>
<!-- Chart.js -->
<script src="/vendors/Chart.js/dist/Chart.min.js"></script>
<!-- gauge.js -->
<script src="/vendors/gauge.js/dist/gauge.min.js"></script>
<!-- bootstrap-progressbar -->
<script src="/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
<!-- iCheck -->
<script src="/vendors/iCheck/icheck.min.js"></script>
<!-- Skycons -->
<script src="/vendors/skycons/skycons.js"></script>
<!-- Flot -->
<script src="/vendors/Flot/jquery.flot.js"></script>
<script src="/vendors/Flot/jquery.flot.pie.js"></script>
<script src="/vendors/Flot/jquery.flot.time.js"></script>
<script src="/vendors/Flot/jquery.flot.stack.js"></script>
<script src="/vendors/Flot/jquery.flot.resize.js"></script>
<!-- Flot plugins -->
<script src="/vendors/flot.orderbars/js/jquery.flot.orderBars.js"></script>
<script src="/vendors/flot-spline/js/jquery.flot.spline.min.js"></script>
<script src="/vendors/flot.curvedlines/curvedLines.js"></script>
<!-- DateJS -->
<script src="/vendors/DateJS/build/date.js"></script>
<!-- JQVMap -->
<script src="/vendors/jqvmap/dist/jquery.vmap.js"></script>
<script src="/vendors/jqvmap/dist/maps/jquery.vmap.world.js"></script>
<script src="/vendors/jqvmap/examples/js/jquery.vmap.sampledata.js"></script>
<!-- bootstrap-daterangepicker -->
<script src="/vendors/moment/min/moment.min.js"></script>
<script src="/vendors/bootstrap-daterangepicker/daterangepicker.js"></script>
@endsection