<?php use App\Lead; ?>
<div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="/" class="site_title"><img src="/images/ula-logo.png" alt="" class="img-responsive" style="width: 115px; margin: 0 auto"> </a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <div class="profile clearfix">
              <div class="profile_pic">
                <img src="/images/img.jpg" alt="..." class="img-circle profile_img">
              </div>
              <div class="profile_info">
                <span>Bienvenido,</span>
                <h2>{{Auth::user()->name}}</h2>
              </div>
              <div class="clearfix"></div>
            </div>
            <!-- /menu profile quick info -->

            <br />

            <!-- sidebar menu -->
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                <h3>Menú</h3>
                <ul class="nav side-menu">
                  <li><a href="/"><i class="fa fa-home"></i> Inicio <span class="fa"></span></a></li>
                  @if(Auth::user()->roll == 1)
                  <li><a href="/usuarios"><i class="fa fa-users"></i> Usuarios </a></li>
                  @endif
                  @if(Auth::user()->roll == 1 || Auth::user()->roll == 3)
                  <li><a href="/amarillas"><i class="fa fa-book"></i> Amarillas </span></a></li>
                  @endif
                  <li><a href="/filtro"><i class="fa fa-edit"></i> CRM </span></a></li>
                  @if(Auth::user()->roll == 1 || Auth::user()->roll == 4)
                  <li><a href="/datos"><i class="fa fa-database"></i> Datos </span></a></li>
                  @endif
                  @if(Auth::user()->roll == 1)
                     <?php
                  $hoy = date("Y-m-d");  
                  $hoy = date("Y-m-d");  
                  $cDatosHoy = Lead::whereIn('estado', [4,9])->whereDate('scheduled','=', $hoy)->orderBy('scheduled')->count(); ?>
                  <li><a href="/pendientes-hoy"><i class="fa fa-list-ul"></i>({{ $cDatosHoy }}) Pendientes para Hoy </span></a></li>
                  @endif
                  @if(Auth::user()->roll == 2)
                  <?php
                  $hoy = date("Y-m-d");  
                  $cDatosHoy = Lead::where('id_user','=',Auth::user()->id)->whereIn('estado', [4,9])->whereDate('scheduled','=', $hoy)->orderBy('scheduled')->count(); ?>
                  <li><a href="/pendientes-hoy"><i class="fa fa-list-ul"></i>({{ $cDatosHoy }}) Pendientes para Hoy </span></a></li>
                  @endif

                  <!-- <li><a href="/estadisticas"><i class="fa fa-bar-chart"></i> Estadisticas </span></a></li> -->
                  <li><a href="/recordatorios"><i class="fa fa-clock-o"></i> Recordatorios </span></a></li>
                  
                </ul>
              </div>
            </div>
            <!-- /sidebar menu -->

            <!-- /menu footer buttons -->
            <div class="sidebar-footer hidden-small">
             
              <a data-toggle="tooltip" data-placement="top" title="Logout" href="/auth/logout">
                <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
              </a>
            </div>
            <!-- /menu footer buttons -->
          </div>
        </div>