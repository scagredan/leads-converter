<?php use App\Lead;
 ?>
@extends('app')
@section('css')
<!-- FullCalendar -->
<link href="/vendors/fullcalendar/dist/fullcalendar.min.css" rel="stylesheet">
<link href="/vendors/fullcalendar/dist/fullcalendar.print.css" rel="stylesheet" media="print">
@endsection
@section('content')
<!-- page content -->
<div class="right_col" role="main">
	<div class="">
		<div class="page-title">
			<div class="title_left">
				<h3>Recordatorios de Leads</h3>
			</div>


		</div>

		<div class="clearfix"></div>

		<div class="row">
			<div class="col-md-12">
				<div class="x_panel">
					<div class="x_title">
						<h2>Eventos de recordatorio</h2>
						<ul class="nav navbar-right panel_toolbox">

							<li><a class="close-link"><i class="fa fa-close"></i></a>
							</li>
						</ul>
						<div class="clearfix"></div>
					</div>
					<div class="col-md-12 col-sm-12 col-xs-12">
						<div class="x_panel">
							<div class="x_title">
								<h2>Proximos eventos</h2>

								<div class="clearfix"></div>
							</div>
							<div class="x_content">

								<div class="">
									<ul class="to_do">
										@foreach($recordatorios as $evento)
										<li>
											<a type="button" class="" style="cursor: pointer;" data-toggle="modal" data-target="#mdl-{{$evento->id}}">
											<p>
												{{$evento->comentario}} || {{$evento->fecha}}</p></a>
											</li>
											@endforeach

										</ul>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	@foreach($recordatorios as $evento)
	<!-- Modal -->
<div id="mdl-{{$evento->id}}" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Evento de recordatorio</h4>
      </div>
      <div class="modal-body">
        <p>{{$evento->comentario}}</p>
        <p>Nombre: {{Lead::find($evento->id_lead)->name}} <br>
        	Teléfono: {{Lead::find($evento->id_lead)->telefono}}</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
	@endforeach
	<!-- /page content -->
	@endsection

	@section('scripts')
	<!-- FullCalendar -->
	<script src="/vendors/moment/min/moment.min.js"></script>
	<script src="/vendors/fullcalendar/dist/fullcalendar.min.js"></script>
	<script>
		/* CALENDAR */

		function  init_calendar() {

			if( typeof ($.fn.fullCalendar) === 'undefined'){ return; }
			console.log('init_calendar');

			var date = new Date(),
			d = date.getDate(),
			m = date.getMonth(),
			y = date.getFullYear(),
			started,
			categoryClass;

			var calendar = $('#calendar').fullCalendar({
				header: {
					left: 'prev,next today',
					center: 'title',
					right: 'month,agendaWeek,agendaDay,listMonth'
				},
				selectable: true,
				selectHelper: true,
				select: function(start, end, allDay) {
					$('#fc_create').click();

					started = start;
					ended = end;

					$(".antosubmit").on("click", function() {
						var title = $("#title").val();
						if (end) {
							ended = end;
						}

						categoryClass = $("#event_type").val();

						if (title) {
							calendar.fullCalendar('renderEvent', {
								title: title,
								start: started,
								end: end,
								allDay: allDay
							},
						  true // make the event "stick"
						  );
						}

						$('#title').val('');

						calendar.fullCalendar('unselect');

						$('.antoclose').click();

						return false;
					});
				},
				eventClick: function(calEvent, jsEvent, view) {
					$('#fc_edit').click();
					$('#title2').val(calEvent.title);

					categoryClass = $("#event_type").val();

					$(".antosubmit2").on("click", function() {
						calEvent.title = $("#title2").val();

						calendar.fullCalendar('updateEvent', calEvent);
						$('.antoclose2').click();
					});

					calendar.fullCalendar('unselect');
				},
				editable: true,
				events: [@foreach($recordatorios as $evento){
					title: '{{$evento->comentario}}',
					start: new Date("{{$evento->fecha}}".replace(/-/g,"/"))
				},@endforeach]
			});

		};


	</script>
	<script>
		$(document).ready(function() {
			init_calendar();
		});
	</script>
	@endsection