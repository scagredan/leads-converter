<?php 	use App\Roll; ?>
@extends('app')
@section('css')
<link href="/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
<link href="/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
<link href="/vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
<link href="/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
<link href="/vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">
<style>
.right_col{
	min-height: auto!important;
}
</style>
@endsection
@section('content')
<!-- page content -->
<div class="right_col" >
	<div class="">
		<div class="page-title">
			<div class="title_left">
				<h3>Usuarios <small>Módulo para gestión de usuarios</small></h3>
			</div>
		</div>

		<div class="clearfix"></div>

		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="x_panel">
					<div class="x_title">
						<h2>Lista</h2>
						<ul class="nav navbar-right panel_toolbox">
							<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
							</li>
						</ul>
						<div class="clearfix"></div>
					</div>
					<div class="x_content">                    
						<table id="datatable" class="table table-striped table-bordered">
							<thead>
								<tr>
									<th>Nombre</th>
									<th>Email</th>
									<th>Tipo de Usuario</th>
									<th>Acciones</th>
								</tr>
							</thead>
							<tbody>
								@foreach($users as $user)
								<tr>
									<td>{{$user->name}}</td>
									<td>{{$user->email}}</td>
									<td>{{$user->getRoll->name}}</td>
									<td>
										<a href="#" class="btn btn-info btn-xs" data-toggle="modal" data-target=".bs-example-modal-lg" onclick="changeModal('{{$user->id}}','{{$user->name}}','{{$user->email}}' ,'{{$user->getRoll->id}}')"><i class="fa fa-pencil"></i> Editar </a>
										<a href="#" class="btn btn-danger btn-xs" data-toggle="modal" data-target=".bs-example-modal-sm"><i class="fa fa-trash-o"></i> Eliminar </a>
									</td>
								</tr>
									@endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">

			<form id="formModal" data-parsley-validate class="form-horizontal form-label-left" method="post">
				<input name="_method" type="hidden" value="PUT">
				<input type="hidden" id="id" name="identificador">
				<input type="hidden" name="_token" value="{{ csrf_token() }}">

				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
					</button>
					<h4 class="modal-title" id="myModalLabel">Editar usuario</h4>
				</div>
				<div class="modal-body">
					<div class="x_panel">
						<div class="x_content">
							<br />
							<div class="form-group">
								<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Nombre <span class="required">*</span>
								</label>
								<div class="col-md-6 col-sm-6 col-xs-12">
									<input type="text" id="name" required="required" class="form-control col-md-7 col-xs-12" name="name">
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Correo <span class="required">*</span>
								</label>
								<div class="col-md-6 col-sm-6 col-xs-12">
									<input type="email" id="email" required="required" class="form-control col-md-7 col-xs-12" name="email">
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Tipo de usuario <span class="required">*</span>
								</label>
								<div class="col-md-6 col-sm-6 col-xs-12">
									<select name="roll" id="roll" class="form-control col-md-7 col-xs-12" name="roll">
										@foreach(Roll::all() as $roll)
										<option value="{{$roll->id}}">{{$roll->name}}</option>
										@endforeach
									</select>
								</div>
							</div>
						</div>
					</div>				
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
					<button type="submit" class="btn btn-primary">Guardar</button>
				</div>
			</form>
		</div>
	</div>
</div>

<div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">

			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
				</button>
				<h4 class="modal-title" id="myModalLabel2">Eliminar Usuario</h4>
			</div>
			<div class="modal-body">
				<h4>Alerta</h4>
				<p>Si se elimina el usuario no se podrá recuperar. ¿Desea continuar?.</p>
				
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
				<button type="button" class="btn btn-primary">Aceptar</button>
			</div>

		</div>
	</div>
</div>

@endsection
@section('scripts')
<!-- Datatables -->
<script src="/vendors/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<script src="/vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
<script src="/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
<script src="/vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
<script src="/vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
<script src="/vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
<script src="/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
<script src="/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
<script src="/vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
<script src="/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
<script src="/vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
<script src="/vendors/jszip/dist/jszip.min.js"></script>
<script src="/vendors/pdfmake/build/pdfmake.min.js"></script>
<script src="/vendors/pdfmake/build/vfs_fonts.js"></script>
<script>
	function changeModal(id, nombre, correo, roll){
		$('#idModal').val(id);
		$('#name').val(nombre);
		$('#email').val(correo);
		$('#roll').val(roll);
		$('#formModal').attr('action', '/usuarios/'+id);
	}
</script>

@endsection